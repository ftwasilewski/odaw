<?php
session_start();
require("valida.php");

require("conexao.php");
// Recupera os itens do banco de dados
$query_consult =  "SELECT * FROM itens";
$result = $connection->query($query_consult);
if(!$result) {
	die("Houve um erro na query de consulta: " . $connection->error);
}
require("desconexao.php");

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>miniERP</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<div class="container-fluid">

		<?php require("cabecalho.php"); ?>

		<?php require("menu.php"); ?>

		<div class="container-fluid">
			<div class="row">
				<div class="col-md-10">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Consultar Item</h3>
						</div>
						<div class="panel-body">
							<div class="container-fluid main-container">
								<form class="form-buscaitem" action="">
									<label>Busca</label>
									<input type="text" data-toggle="dropdownmenu" id="inputBusca" name="inputBusca" class="form-control" placeholder="código, descrição, ou qualquer outra informação" autofocus onkeyup="buscarItens(this.value)">
									<ul id="livesearch">
									</ul>
								</form>


								<form class="form-cruditem" method="post" action="cruditem.php">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>Código</th>
												<th>Descrição</th>
												<th>Modelo</th>
												<th>Marca</th>
												<th>Custo R$</th>
												<th>Venda R$</th>
											</tr>
										</thead>
										<tbody id="table-item">
											<tr>
												<td><input type="text" class="form-control" name="inputCodigo" id="inputCodigo" disabled></td>
												<td><input type="text" class="form-control editable" name="inputDescricao" id="inputDescricao" disabled></td>
												<td><input type="text" class="form-control editable" name="inputModelo" id="inputModelo" disabled></td>
												<td><input type="text" class="form-control editable" name="inputMarca" id="inputMarca" disabled></td>
												<td><input type="text" class="form-control editable" name="inputCusto" id="inputCusto" disabled></td>
												<td><input type="text" class="form-control editable" name="inputVenda" id="inputVenda" disabled></td>
											</tr>
										</tbody>
									</table>
									<div class="row">
										<div class="col-md-2 col-md-offset-4">
											<button class="btn btn-lg btn-primary btn-block" type="button" name="buttonModificar" id="buttonModificar">Modificar</button>
										</div>
										<div class="col-md-2">
											<button class="btn btn-lg btn-primary btn-block" type="button" name="buttonExcluir" id="buttonExcluir">Excluir</button>
										</div>
									</div>
								</form>

							</div>
						</div>
					</div>
					
				</div>
				<div class="col-md-2 aside">Propagandas</div>
			</div>
		</div>

		<?php require("rodape.php"); ?>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery-1.11.3.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>

	<script src="js/cruditem.js"></script>
</body>
</html>