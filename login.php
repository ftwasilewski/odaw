<?php
// Verifica se o formulário foi submetido
if(isset($_POST['inputEmail']) && $_POST['inputEmail'] != "") {
	// Conecta ao banco de dados
	require("conexao.php");

	// $db_select = mysqli_select_db($connection, "odaw");
	// if (!$db_select) {
	// 	die("Base não pode ser selecionada: ". mysqli_error());
	// }

	// Coloca as variáveis do $_POST em variáveis
	$email = $_POST['inputEmail'];
	$senha = md5($_POST['inputSenha']);

	// Consulta o usuário e senha no banco de dados
	$query_consult =  "SELECT * FROM usuarios WHERE email='".$email."' AND senha='".$senha."'";
	$result = $connection->query($query_consult);
	if(!$result) {
		die("Houve um erro na query de consulta: " . $connection->error);
	}

	if($result->num_rows == 0) {
		echo '<script>alert("Login e/ou senha inválido!")</script>';
	} elseif ($result->num_rows == 1) {
		echo '<script>alert("Login correto")</script>';
		$row = $result->fetch_assoc();
		session_start();
		$_SESSION['id'] = $row["id"];
		$_SESSION['nome'] = $row["nome"];
		$_SESSION['sobrenome'] = $row["sobrenome"];
		$_SESSION['privilegio'] = $row["privilegio"];
		$_SESSION['email'] = $row["email"];


		header("Location: index.php");
	}
	require("desconexao.php");
}
?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>miniERP</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="css/login.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<form class="form-signin" method="post" action="">
			<h2 class="form-signin-heading">miniERP Login</h2>
			<input type="email" id="inputEmail" name="inputEmail" class="form-control" placeholder="Email" required autofocus>
			<input type="password" id="inputPassword" name="inputSenha" class="form-control" placeholder="Senha" required>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Logar</button>
		</form>
	</div> <!-- /container -->
</body>
</html>
