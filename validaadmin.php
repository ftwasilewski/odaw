<?php
if (!isset($_SESSION['privilegio']) || $_SESSION['privilegio'] != 1) {
	?>
	<script type="text/javascript">
	alert("Apenas administradores podem acessar esta página!");
	window.location.href = "index.php";
	</script>
	<?php
	// header("Location: index.php");
	exit;
}
?>