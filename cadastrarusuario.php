<?php
session_start();
require("valida.php");
require("validaadmin.php"); 
?>

<?php
// Verifica se o formulário foi submetido
if(isset($_POST['inputEmail']) && $_POST['inputEmail'] != "") {
	// Conecta ao banco de dados
	require("conexao.php");

	// $db_select = mysqli_select_db($connection, "odaw");
	// if (!$db_select) {
	// 	die("Base não pode ser selecionada: ". mysqli_error());
	// }

	// Coloca as variáveis do $_POST em variáveis
	$nome = $_POST['inputNome'];
	$sobrenome = $_POST['inputSobrenome'];
	$privilegio = intval($_POST['inputPrivilegio']);
	$email = $_POST['inputEmail'];
	$senha = md5($_POST['inputSenha']);
	//$senha = $_POST['inputSenha'];

	// Verifica se o usuário já existe
	$query_consult =  "SELECT * FROM usuarios WHERE email='".$email."'";
	$result = $connection->query($query_consult);
	if(!$result) {
		die("Houve um erro na query de consulta: " . $connection->error);
	}
	if($result->num_rows > 0) {
		echo '<script>alert("Usuário existente!")</script>';
		// while ($row = $result->fetch_assoc()) {
		// 	echo $row["nome"];
		// }
	} else {
// $stmt = $conn->prepare("INSERT INTO MyGuests (firstname, lastname, email) VALUES (?, ?, ?)");
// $stmt->bind_param("sss", $firstname, $lastname, $email);
		//Prepara e bind o statement
		$stmt = $connection->prepare("INSERT INTO usuarios (nome, sobrenome, privilegio, email, senha) VALUES (?,?,?,?,?)");
		$stmt->bind_param("ssiss", $nome, $sobrenome, $privilegio, $email, $senha);

		//Insere no banco de dados
		$stmt->execute();
		$stmt->close();


		// Inclui no banco de dados
		// $query_insert = "INSERT INTO usuarios(`nome`, `sobrenome`, `privilegio`, `email`, `senha`) VALUES ('".$nome."','".$sobrenome."','".$privilegio."','".$email."','".$senha."')";
		// if(!$connection->query($query_insert)) {
		// 	die("Houve um erro na query de inserção: " . $connection->error);
		// }
		echo '<script>alert("Cadastro efetuado com sucesso!")</script>';
	 	// header("Location: index.php");
	}

	require("desconexao.php");
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>miniERP</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<div class="container-fluid">

		<?php require("cabecalho.php"); ?>

		<?php require("menu.php"); ?>

		<div class="container-fluid">
			<div class="row">
				<div class="col-md-10">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Cadastrar Usuário</h3>
						</div>
						<div class="panel-body">
							<div class="container-fluid main-container">
								<form class="form-cadusuario" method="post" action="">
									<label>Nome</label>
									<input type="text" id="inputNome" name="inputNome" class="form-control" required autofocus>

									<label>Sobrenome</label>
									<input type="text" id="inputSobrenome" name="inputSobrenome" class="form-control" required>

									<label>Privilégio</label>
									<div class="radio">
										<label><input type="radio" id="inputPrivilegio" name="inputPrivilegio" value="1">Administrador</label>
									</div>
									<div class="radio">
										<label><input type="radio" id="inputPrivilegio" name="inputPrivilegio" value="2">Vendedor</label>
									</div>

									<label>Email</label>
									<input type="email" id="inputEmail" name="inputEmail" class="form-control" required>

									<label>Senha</label>
									<input type="password" id="inputSenha" name="inputSenha" class="form-control" required>

									<button class="btn btn-lg btn-primary btn-block" type="submit">Cadastrar</button>
								</form>
							</div>
						</div>
					</div>
					
				</div>
				<div class="col-md-2 aside">Propagandas</div>
			</div>
		</div>

		<?php require("rodape.php"); ?>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery-1.11.3.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
</body>
</html>