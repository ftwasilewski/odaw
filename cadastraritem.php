<?php
session_start();
require("valida.php");
?>

<?php
// Verifica se o formulário foi submetido
if(isset($_POST['inputDescricao']) && $_POST['inputDescricao'] != "") {
	// Conecta ao banco de dados
	require("conexao.php");

	// Coloca as variáveis do $_POST em variáveis
	$descricao = $_POST['inputDescricao'];
	$modelo = $_POST['inputModelo'];
	$marca = $_POST['inputMarca'];
	$custo = doubleval($_POST['inputCusto']);
	$venda = doubleval($_POST['inputVenda']);


	$stmt = $connection->prepare("INSERT INTO itens (descricao, modelo, marca, pcusto, pvenda) VALUES (?,?,?,?,?)");
	$stmt->bind_param("sssdd", $descricao, $modelo, $marca, $custo, $venda);

	$stmt->execute();
	$stmt->close();

	echo '<script>alert("Cadastro efetuado com sucesso!")</script>';


	require("desconexao.php");
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>miniERP</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<div class="container-fluid">

		<?php require("cabecalho.php"); ?>

		<?php require("menu.php"); ?>

		<div class="container-fluid">
			<div class="row">
				<div class="col-md-10">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Cadastrar Item</h3>
						</div>
						<div class="panel-body">
							<div class="container-fluid main-container">
								<form class="form-caditem" method="post" action="">

									<div class="row">
										<label class="col-md-9 col-md-offset-3" >Descrição</label>
										<div class="col-md-6 col-md-offset-3"><input type="text" class="form-control" name="inputDescricao" required autofocus></div>
									</div>

									<div class="row">
										<div class="col-md-3 col-md-offset-3">
											<label class="label-cadastro">Modelo</label>
											<input type="text" class="form-control" name="inputModelo">
										</div>
										<div class="col-md-3" >
											<label class="label-cadastro">Marca</label>
											<input type="text" class="form-control" name="inputMarca">
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-3 col-md-offset-3">
											<label class="label-cadastro">Preço Custo</label>
											<input type="text" class="form-control" name="inputCusto" placeholder="ex: 1234.56">
										</div>
										<div class="col-md-3" >
											<label class="label-cadastro">Preço Venda</label>
											<input type="text" class="form-control" name="inputVenda" placeholder="ex: 1234.56">
										</div>
									</div>

									<div class="row">
										<div class="col-md-6 col-md-offset-3 botao-cadastro">
											<button class="btn btn-lg btn-primary btn-block" type="submit">Cadastrar</button>
										</div>
									</div>

								</form>
							</div>
						</div>
					</div>
					
				</div>
				<div class="col-md-2 aside">Propagandas</div>
			</div>
		</div>

		<?php require("rodape.php"); ?>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery-1.11.3.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
</body>
</html>