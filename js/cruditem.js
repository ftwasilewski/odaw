$(document).ready(function(){
  $('#buttonExcluir').on('click',function(){
    console.log("Botao exluir pressionado");
    $.ajax({
      url: 'cruditem.php',
      type: 'post',
      data: {'inputExcluir': 'pressed',
      'inputCodigo': $("#inputCodigo").val(),
      'inputDescricao': $("#inputDescricao").val(),
      'inputModelo': $("#inputModelo").val(),
      'inputMarca': $("#inputMarca").val(),
      'inputCusto': $("#inputCusto").val(),
      'inputVenda': $("#inputVenda").val()},
      success: function(data, status) {
        console.log(data);
        if (data == "OK") {
          alert("Item exluído com sucesso");
          window.location.href = "consultaritem.php";
        } else {
          alert(data);
          window.location.href = "consultaritem.php";
        }
      },
      error: function(xhr, desc, err) {
        console.log(xhr);
        console.log("Details: " + desc + "\nError:" + err);
      }
    });
  });

  $('#buttonModificar').on('click',function(){
    console.log("Botao modificar pressionado");
    $.ajax({
      url: 'cruditem.php',
      type: 'post',
      data: {'inputModificar': 'pressed',
      'inputCodigo': $("#inputCodigo").val(),
      'inputDescricao': $("#inputDescricao").val(),
      'inputModelo': $("#inputModelo").val(),
      'inputMarca': $("#inputMarca").val(),
      'inputCusto': $("#inputCusto").val(),
      'inputVenda': $("#inputVenda").val()},
      success: function(data, status) {
        console.log(data);
        if (data == "OK") {
          alert("Item modificado com sucesso");
          window.location.href = "consultaritem.php";
        } else {
          alert(data);
          window.location.href = "consultaritem.php";
        }
      },
      error: function(xhr, desc, err) {
        console.log(xhr);
        console.log("Details: " + desc + "\nError:" + err);
      }
    });
  });

});




function buscarItens(inputBuscar) {
  $.ajax({
    url: 'cruditem.php',
    type: 'post',
    data: {'inputBuscar': inputBuscar},
    success: function(data, status) {
      $("#livesearch").html("");
      $("#livesearch").append(data);
    },
    error: function(xhr, desc, err) {
      console.log(xhr);
      console.log("Details: " + desc + "\nError:" + err);
    }
  });
}

function mostrarItemCompleto(codItem){
  console.log(codItem);
  $("#livesearch").html("");
  $.ajax({
    url: 'cruditem.php',
    type: 'post',
    data: {'codItem': codItem},
    success: function(data, status) {
      var item = $.parseJSON(data);

      $(".editable").removeAttr('disabled');
      $("#inputCodigo").val(item["codigo"]);
      $("#inputDescricao").val(item["descricao"]);
      $("#inputModelo").val(item["modelo"]);
      $("#inputMarca").val(item["marca"]);
      $("#inputCusto").val(item["pcusto"]);
      $("#inputVenda").val(item["pvenda"]);
    },
    error: function(xhr, desc, err) {
      console.log(xhr);
      console.log("Details: " + desc + "\nError:" + err);
    }
  });
}

function excluirItem(e) {
  e.preventDefault();
  console.log("oi");
}