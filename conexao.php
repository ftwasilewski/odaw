<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "minierp";

$connection = mysqli_connect($servername, $username, $password, $database);
	// verifica a conexao com o banco
if ($connection->connect_error > 0) {
	die("Falha de Conexão: " . $connection->connect_error);
}

	//especifica o charset do cliente - usar o mesmo especificado para a página
$connection->set_charset("utf8");
?>