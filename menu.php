<nav class="navbar navbar-default">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#colapse-menu" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">miniERP</a>
		</div>

		<div class="collapse navbar-collapse " id="colapse-menu">
			<ul class="nav navbar-nav">
				<li class="dropdown-estoque">
					<a href="#" data-toggle="dropdown" role="button">Estoque <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="cadastraritem.php">Cadastrar Item</a></li>
						<li><a href="consultaritem.php">Consultar/Excluir Item</a></li>
						<li><a href="relatorioitem.php">Relatório de Itens</a></li>
					</ul>
				</li>
				<li class="dropdown-venda">
					<a href="#" data-toggle="dropdown" role="button">Venda <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="gerarorcamento.php">Gerar Orçamento</a></li>
						<li><a href="consultarorcamento.php">Consultar Orçamento</a></li>
						<li><a href="relatorioorcamento.php">Relatório de Orçamentos</a></li>
					</ul>
				</li>
				<li class="dropdown-recursoshumanos">
					<a href="#" data-toggle="dropdown" role="button">Recursos Humanos <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="cadastrarusuario.php">Cadastrar Usuário</a></li>
						<li><a href="consultarusuario.php">Consultar/Excluir Usuário</a></li>
						<li><a href="relatoriousuario.php">Relatório de Usuários</a></li>
					</ul>
				</li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown-funcionario">
					<a href="#" data-toggle="dropdown" role="button">Olá <?php echo $_SESSION["nome"]?><span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="meusdados.php">Meus dados</a></li>
						<li><a href="logout.php">Logout</a></li>
					</ul>
				</li>
			</ul>

		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>