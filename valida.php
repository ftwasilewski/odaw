<?php
if (!isset($_SESSION['id']) || !isset($_SESSION['nome']) || !isset($_SESSION['privilegio'])) {
	header("Location: login.php");
	exit;
}
?>