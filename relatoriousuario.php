<?php
session_start();
require("valida.php");
require("validaadmin.php");

require("conexao.php");
// Recupera os usuários do banco de dados
$query_consult =  "SELECT * FROM usuarios";
$result = $connection->query($query_consult);
if(!$result) {
	die("Houve um erro na query de consulta: " . $connection->error);
}
require("desconexao.php");

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>miniERP</title>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<div class="container-fluid">

		<?php require("cabecalho.php"); ?>

		<?php require("menu.php"); ?>

		<div class="container-fluid">
			<div class="row">
				<div class="col-md-10">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Relatório de Usuários</h3>
						</div>
						<div class="panel-body">
							<div class="container-fluid main-container">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Nome</th>
											<th>Sobrenome</th>
											<th>Privilégio</th>
											<th>Email</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if($result->num_rows > 0) {
											while ($row = $result->fetch_assoc()) {
												echo "<tr>";
												echo "<td>".$row['nome']."</td>";
												echo "<td>".$row['sobrenome']."</td>";
												echo "<td>".$row['privilegio']."</td>";
												echo "<td>".$row['email']."</td>";
												echo "</tr>";
											}
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					
				</div>
				<div class="col-md-2 aside">Propagandas</div>
			</div>
		</div>

		<?php require("rodape.php"); ?>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery-1.11.3.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
</body>
</html>